# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## 1.0.2 (2020-03-05)


### Features

* **ds-button:** added new icon prop ([25e25e8](https://bitbucket.org/arodrigues-dev/ds-monorepo/commits/25e25e82f914e13aaeb565b7e403c53facb82788))


### BREAKING CHANGES

* **ds-button:** onFocus prop was removed, new icon prop added





## 1.0.1 (2020-03-04)

### Features

* **bootstrap:** initial project bootstrap commits ([01caf26](https://github.com/antonio-rodrigues/ds-monorepo/commit/01caf2654366ad96c4092df550117cfb6c8758f6))
