import React from 'react'

const DsIcon = icon => (<i icon={icon}></i>)

export const DsButton = ({ value, className, icon = null, onClick, children }) => (
  <button className={className} onClick={onClick} value={value}>
    {children}
    {icon && <DsIcon icon={icon} />}
  </button>
);
