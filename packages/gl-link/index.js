import React from 'react'

export const DsLink = ({ href, target, className, onClick, text = null }) => (
  <a href={href} target={target} onClick={onClick} className={className}>
    {text || href}
  </a>
);
